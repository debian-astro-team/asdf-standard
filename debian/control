Source: asdf-standard
Maintainer: Debian Astronomy Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               pybuild-plugin-pyproject,
               python3-all,
               python3-asdf <!nocheck>,
               python3-asdf-astropy <!nocheck>,
               python3-astropy <!nocheck>,
               python3-gwcs <!nocheck>,
               python3-jsonschema <!nocheck>,
               python3-pytest <!nocheck>,
               python3-setuptools,
               python3-setuptools-scm,
               python3-yaml <!nocheck>
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/debian-astro-team/asdf-standard
Vcs-Git: https://salsa.debian.org/debian-astro-team/asdf-standard.git
Homepage: https://github.com/asdf-format/asdf-standard
Rules-Requires-Root: no

Package: python3-asdf-standard
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Description: Python package describing the Advanced Scientific Data Format
 ASDF (Advanced Scientific Data Format) is a proposed
 next generation interchange format for scientific data. ASDF aims to
 exist in the same middle ground that made FITS so successful, by
 being a hybrid text and binary format: containing human editable
 metadata for interchange, and raw binary data that is fast to load
 and use. Unlike FITS, the metadata is highly structured and is
 designed up-front for extensibility.
 .
 This package provides the ASDF standard schemas.
